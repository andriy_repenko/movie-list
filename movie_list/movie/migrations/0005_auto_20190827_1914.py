# Generated by Django 2.2.4 on 2019-08-27 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0004_auto_20190827_1811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='poster',
            field=models.ImageField(upload_to='movie_posters'),
        ),
    ]
