import datetime
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth.models import User


def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class Genre(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name


class Movie(models.Model):
    name = models.CharField(max_length=2014, null=False, blank=False)
    description = models.TextField(null=False, blank=False)
    year = models.PositiveIntegerField(
        default=current_year(), validators=[MinValueValidator(1895), max_value_current_year], null=False, blank=False)
    poster = models.ImageField(upload_to='movie_posters')
    # TODO change to PositiveIntegerField
    length = models.IntegerField(default=80, validators=[MaxValueValidator(999), MinValueValidator(1)], null=False,
                                 blank=False)
    genre = models.ManyToManyField(Genre)

    def __str__(self):
        return self.name


class Review(models.Model):
    rating = models.IntegerField(default=10, validators=[MaxValueValidator(10), MinValueValidator(1)])
    comment = models.TextField()
    user = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE)

    def __str__(self):
        return '{0} rating for {1} by {2} {3}'.format(self.rating, self.movie.name, self.user.first_name,
                                                      self.user.last_name)
