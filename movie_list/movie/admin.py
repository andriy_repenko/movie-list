from django.contrib import admin

from movie_list.movie.models import Movie, Genre


class MovieAdmin(admin.ModelAdmin):
    list_display = ('name', 'year', 'length', )
    list_filter = ('year', )


admin.site.register(Movie, MovieAdmin)
admin.site.register(Genre)
